const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const webpack = require("webpack");

module.exports = {
    mode: "development",
    entry: {
        main: path.resolve(__dirname, './src/main.js'),
        bootstrap: path.resolve(__dirname, './src/bootstrap.js'),
        jquery: path.resolve(__dirname, './src/jquery.js'),
        flatpickr: path.resolve(__dirname, './src/flatpickr.js'),
    },
    output: {
        path: path.resolve(__dirname, './public'),
        filename: '[name].js',
        globalObject: "this",
        publicPath: "/public",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: '/node_modules/'
            },
            {
                test: /\.css/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        })
    ]
}