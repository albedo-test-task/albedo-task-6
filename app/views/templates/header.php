<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="public/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="public/flatpickr.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="public/main.css" />

    <title>Conference</title>
  </head>
  <body>
  
	<ul class="nav justify-content-end">
		<li class="nav-item">
			<a class="nav-link" href="/">Main</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/members">Conference members</a>
		</li>
	</ul>