	<!-- JavaScript and dependencies -->
  <script src="public/bootstrap.js"></script>
  <script src="public/jquery.js"></script>
  <script src="public/flatpickr.js"></script>

  <!-- Our scripts -->
  <script src="public/main.js"></script>
  
  <script>
	flatpickr("input[type=datetime-local]", {
		enableTime: true,
		dateFormat: "Y-m-d",
	});
  </script>
  </body>
</html>